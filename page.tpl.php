<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >

<head>
 <title><?php print $head_title ?></title>
    <?php print $head ?>
    <?php print $styles ?>
    <?php print $scripts ?>
</head>
<body>
<div id="wrapper">

<div>
	<div id="header">
		<div id="menu">
      <?php if (isset($primary_links)) : ?>
        <?php print theme('links', $primary_links, array('class' => 'links primary-links')) ?>
      <?php endif; ?>
			
		</div>
		<div id="search-container">
		  <?php if ($search_box): ?>
		    <?php print $search_box ?>
      <?php endif; ?>
				
		</div>
	</div>
</div>
<hr />

<!-- start page -->
<div id="page">


	<div id="content">
	<div class="navigation"> <?php print $breadcrumb ?> </div>
    <?php if ($messages != ""): ?>
    	<div id="message"><?php print $messages ?></div>
    <?php endif; ?>
    <?php if ($mission != ""): ?>
    	<div id="mission"><?php print $mission ?></div>
    <?php endif; ?>
    <?php if ($title != ""): ?>
    	<h2 class="page-title"><?php print $title ?></h2>
    <?php endif; ?>
    <?php if ($tabs != ""): ?>
    	<?php print $tabs ?>
    <?php endif; ?>
    <?php if ($help != ""): ?>
    	<p id="help"><?php print $help ?></p>
    <?php endif; ?>
    <!-- start main content -->
    <?php print($content) ?>
    <!-- end main content -->
		
	</div>

	<div id="sidebar">
	  <div id="logo">
      <h1><a href="<?php print url() ?>" title="<?php print($site_name) ?>"><?php print($site_name) ?></a></h1>
      <div class="slogan"><?php print($site_slogan) ?></div>
    </div>
    <div id="sidebar-conent">
    <?php print $right; ?> 
    </div>
	</div>
	<div style="clear: both;">&nbsp;</div>

</div>
</div>
	<div style="clear: both;">&nbsp;</div>
<div id="footer">
	<p class="legal">Pinkish is proudly powered by <a href="http://Drupal.org/">Drupal</a>
		| <a href="#">Entries (RSS)</a>
		| <a href="#">Comments (RSS)</a>. Designed by <a href="http://www.neokrish.co.in/">Neokrish Online</a></p>

</div>

</div>
<!-- end page -->

</body>
</html>
